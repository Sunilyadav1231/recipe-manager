USE `recipedb`;

/*Table structure for table `ingredient` */


CREATE TABLE IF NOT EXISTS `ingredient` (
  `id` varchar(255) NOT NULL,
  `created_datetime` datetime(6) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `retired` int NOT NULL,
  `modified_datetime` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `recipe` */


CREATE TABLE IF NOT EXISTS `recipe` (
  `id` varchar(255) NOT NULL,
  `created_datetime` datetime(6) NOT NULL,
  `retired` int NOT NULL,
  `modified_datetime` datetime(6) DEFAULT NULL,
  `instructions` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `number_of_serving` int DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `recipe_ingredients` */


CREATE TABLE IF NOT EXISTS `recipe_ingredients` (
  `id` varchar(255) NOT NULL,
  `created_datetime` datetime(6) NOT NULL,
  `retired` int NOT NULL,
  `modified_datetime` datetime(6) DEFAULT NULL,
  `ingredient_code` varchar(255) DEFAULT NULL,
  `recipe_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKoldha09rvjdqkm9gcddhqhncs` (`ingredient_code`),
  KEY `FKhnsmvxdlwxqq6x2wbgnoef5gr` (`recipe_id`),
  CONSTRAINT `FKhnsmvxdlwxqq6x2wbgnoef5gr` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  CONSTRAINT `FKoldha09rvjdqkm9gcddhqhncs` FOREIGN KEY (`ingredient_code`) REFERENCES `ingredient` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
