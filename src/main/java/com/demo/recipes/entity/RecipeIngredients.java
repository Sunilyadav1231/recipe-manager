package com.demo.recipes.entity;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "recipe_ingredients")
@Where(clause = "retired = '0'")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class RecipeIngredients extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    private Recipe recipe;

    @ManyToOne
    @JoinColumn(name = "ingredient_code")
    private Ingredient ingredient;

}
