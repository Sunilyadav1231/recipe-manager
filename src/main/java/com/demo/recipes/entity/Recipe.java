package com.demo.recipes.entity;

import com.demo.recipes.enums.RecipeType;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "recipe")
@Where(clause = "retired = '0'")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Recipe extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "instructions")
    private String instructions;

    @Column(name = "number_of_serving")
    private Integer numberOfServing;


    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private RecipeType type;

}
