package com.demo.recipes.mapper;

import com.demo.recipes.entity.Ingredient;
import com.demo.recipes.entity.Recipe;
import com.demo.recipes.entity.RecipeIngredients;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import com.demo.recipes.repository.RecipeIngredientRepository;
import com.demo.recipes.service.IngredientService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class RecipeMapper {

    private final ModelMapper mapper;

    private final IngredientService ingredientService;

    private final RecipeIngredientRepository recipeIngredientRepository;

    public RecipeMapper(ModelMapper mapper, IngredientService ingredientService, RecipeIngredientRepository recipeIngredientRepository) {
        this.mapper = mapper;
        this.ingredientService = ingredientService;
        this.recipeIngredientRepository = recipeIngredientRepository;
    }

    public Recipe getRecipeEntity(RecipeRequest request) {
        return  mapper.map(request, Recipe.class);
    }

    public void mapRecipeEntity(RecipeRequest request, Recipe recipe) {
        mapper.map(request, recipe);
    }

    public Set<RecipeIngredients> mapRecipeIngredientsEntity(Set<Ingredient> ingredients,Recipe recipe) {
        return ingredients.stream().map(it -> RecipeIngredients.builder().recipe(recipe).ingredient(it).build()).collect(Collectors.toSet());
    }

    public RecipeResponse getRecipeResponse(Recipe recipe) {
        RecipeResponse recipeResponse = mapper.map(recipe, RecipeResponse.class);
        List<RecipeIngredients> recipeIngredientsList = recipeIngredientRepository.findByRecipe(recipe);
        if (!CollectionUtils.isEmpty(recipeIngredientsList)) {
            recipeResponse.setIngredients(recipeIngredientsList.stream().map(it -> it.getIngredient().getName()).collect(Collectors.toList()));
        }
        return recipeResponse;
    }
}
