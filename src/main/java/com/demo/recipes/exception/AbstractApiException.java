package com.demo.recipes.exception;

import com.demo.recipes.model.ErrorResponse;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public abstract class AbstractApiException extends RuntimeException{
    private static final Logger log = LoggerFactory.getLogger(AbstractApiException.class);
    private static final long serialVersionUID = 4054883770161866123L;
    private ErrorResponse error;

    public AbstractApiException(ErrorResponse error) {
        this.error = error;
    }

    public AbstractApiException(ErrorResponse error, String msg, Throwable cause) {
        super(msg, cause);
        this.error = error;
        log.info(msg);
    }

    public ErrorResponse getApiError() {
        return this.error;
    }

    public void setApiError(ErrorResponse error) {
        this.error = error;
    }

    public abstract int getHttpStatusCode();
}
