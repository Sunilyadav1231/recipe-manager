package com.demo.recipes.exception;

import com.demo.recipes.model.ErrorResponse;
import org.springframework.http.HttpStatus;

public class ApiBusinessException extends AbstractApiException{
    private static final long serialVersionUID = 2796453897192710255L;
    private final int httpStatusCode;


    public ApiBusinessException(HttpStatus httpStatus,String errorCode, String msg) {
        super(ErrorResponse.builder().code(errorCode).message(msg).build(), msg, null);
        this.httpStatusCode = httpStatus.value();
    }

    public int getHttpStatusCode() {
        return this.httpStatusCode;
    }
}
