package com.demo.recipes.exception.handler;

import com.demo.recipes.exception.ApiBusinessException;
import com.demo.recipes.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.List;

@RestControllerAdvice
@Slf4j
public class GlobalControllerExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorResponse> systemException(Exception exception) {
        if (exception != null) {
            log.error(ExceptionUtils.getStackTrace(exception));
        }

        if (exception instanceof HttpRequestMethodNotSupportedException) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        } else if (exception instanceof ApiBusinessException) {
            return ResponseEntity.status(((ApiBusinessException) exception).getHttpStatusCode()).body(((ApiBusinessException) exception).getError());
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorResponse("9999", "Something went wrong"));
        }
    }

    @ExceptionHandler(ConversionFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleConversion(RuntimeException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponse> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        log.warn(fieldErrors.toString());
        return ResponseEntity.badRequest().body(this.processFieldErrors(fieldErrors));
    }

    @ExceptionHandler({MissingServletRequestParameterException.class, ConstraintViolationException.class, HttpMessageNotReadableException.class})
    public ResponseEntity<ErrorResponse> invalidException(Exception ex) {
        log.warn(ex.getMessage());
        return ResponseEntity.badRequest().body(new ErrorResponse("4000", ex.getMessage()));
    }

    private ErrorResponse processFieldErrors(List<FieldError> fieldErrors) {
        ErrorResponse error = ErrorResponse.builder().code("4001").message("Validation failed").build();
        fieldErrors.forEach(fieldError -> error.addFieldError(fieldError.getField(), fieldError.getDefaultMessage()));
        return error;
    }
}
