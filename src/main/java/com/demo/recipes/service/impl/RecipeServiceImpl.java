package com.demo.recipes.service.impl;

import com.demo.recipes.entity.Ingredient;
import com.demo.recipes.entity.Recipe;
import com.demo.recipes.entity.RecipeIngredients;
import com.demo.recipes.exception.ApiBusinessException;
import com.demo.recipes.mapper.RecipeMapper;
import com.demo.recipes.model.RecipeFilter;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import com.demo.recipes.repository.RecipeIngredientRepository;
import com.demo.recipes.repository.RecipeRepository;
import com.demo.recipes.repository.RecipeRepositoryImpl;
import com.demo.recipes.service.IngredientService;
import com.demo.recipes.service.RecipeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class RecipeServiceImpl implements RecipeService {

    private final RecipeMapper recipeMapper;
    private final RecipeRepository recipeRepository;
    private final RecipeRepositoryImpl recipeRepositoryImpl;
    private final RecipeIngredientRepository ingredientRepository;
    private final IngredientService ingredientService;

    public RecipeServiceImpl(RecipeMapper recipeMapper, RecipeRepository recipeRepository, RecipeRepositoryImpl recipeRepositoryImpl, RecipeIngredientRepository ingredientRepository, IngredientService ingredientService) {
        this.recipeMapper = recipeMapper;
        this.recipeRepository = recipeRepository;
        this.recipeRepositoryImpl = recipeRepositoryImpl;
        this.ingredientRepository = ingredientRepository;
        this.ingredientService = ingredientService;
    }

    @Override
    @Transactional
    public String createRecipe(RecipeRequest request) {
        Recipe recipe = recipeRepository.save(recipeMapper.getRecipeEntity(request));
        Set<Ingredient> ingredients = ingredientService.getIngredients(request.getIngredients());
        Set<RecipeIngredients> recipeIngredients = ingredients.stream().map(it -> RecipeIngredients.builder().recipe(recipe).ingredient(it).build()).collect(Collectors.toSet());
        ingredientRepository.saveAll(recipeIngredients);
        return recipe.getId();
    }

    @Override
    @Transactional
    public void updateRecipe(String id, RecipeRequest request) {
        Recipe recipe = getRecipe(id);

        Set<Ingredient> ingredients = ingredientService.getIngredients(request.getIngredients());
        recipeMapper.mapRecipeEntity(request, recipe);
        ingredientRepository.deleteByRecipe(recipe);
        ingredientRepository.saveAll(recipeMapper.mapRecipeIngredientsEntity(ingredients, recipe));
        recipeRepository.save(recipe);
    }

    @Override
    @Transactional
    public void deleteRecipe(String id) {
        Recipe recipe = getRecipe(id);
        recipe.setRetired(Boolean.TRUE);

        recipeRepository.save(recipe);
    }

    @Override
    public RecipeResponse getRecipeById(String id) {
        return recipeMapper.getRecipeResponse(getRecipe(id));
    }

    @Override
    public Page<RecipeResponse> getRecipes(Pageable pageable, RecipeFilter recipeFilter) {
        if (pageable == null) {
            throw new ApiBusinessException(HttpStatus.BAD_REQUEST, "4015", "Page information not available");
        }
        if (StringUtils.hasText(recipeFilter.getIngredientExclude())) {
            recipeFilter.setIngredientExclude(recipeFilter.getIngredientExclude().trim().replace(" ", "_").toUpperCase(Locale.ROOT));
        }
        if (StringUtils.hasText(recipeFilter.getIngredientInclude())) {
            recipeFilter.setIngredientInclude(recipeFilter.getIngredientInclude().trim().replace(" ", "_").toUpperCase(Locale.ROOT));
        }

        Page<Recipe> recipes = recipeRepositoryImpl.getRecipes(recipeFilter, pageable);
        if (CollectionUtils.isEmpty(recipes.getContent())) {
            return new PageImpl<>(Collections.emptyList(), pageable, 0);
        }
        return mapRecipeEntityToDto(pageable, recipes);
    }

    private Page<RecipeResponse> mapRecipeEntityToDto(Pageable pageable, Page<Recipe> recipes) {
        List<RecipeResponse> recipeResponses = recipes.getContent().stream().map(recipeMapper::getRecipeResponse).collect(Collectors.toList());
        return new PageImpl<>(recipeResponses, pageable, recipes.getTotalElements());
    }

    public Recipe getRecipe(String id) {
        Optional<Recipe> recipeOpt = recipeRepository.findById(id);
        if (recipeOpt.isPresent()) {
            return recipeOpt.get();
        } else {
            throw new ApiBusinessException(HttpStatus.NOT_FOUND, "4004", "requested resource not found");
        }
    }
}
