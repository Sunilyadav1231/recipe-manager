package com.demo.recipes.service.impl;

import com.demo.recipes.entity.Ingredient;
import com.demo.recipes.exception.ApiBusinessException;
import com.demo.recipes.repository.IngredientRepository;
import com.demo.recipes.service.IngredientService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class IngredientServiceImpl implements IngredientService {

    private final IngredientRepository ingredientRepository;

    public IngredientServiceImpl(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public Set<Ingredient> getIngredients(List<String> ingredientStrings) {
        if(CollectionUtils.isEmpty(ingredientStrings)){
            throw new ApiBusinessException(HttpStatus.BAD_REQUEST,"4010","Invalid Request");
        }
        Set<Ingredient> ingredients = ingredientStrings.stream().map(it -> Ingredient.builder().id(it.trim().replace(" ", "_").toUpperCase(Locale.ROOT)).name(it).build()).collect(Collectors.toSet());
        return new HashSet<>(ingredientRepository.saveAll(ingredients));
    }
}
