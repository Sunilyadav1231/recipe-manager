package com.demo.recipes.service;

import com.demo.recipes.entity.Ingredient;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface IngredientService {

    Set<Ingredient> getIngredients(List<String> ingredients);
}
