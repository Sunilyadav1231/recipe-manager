package com.demo.recipes.service;

import com.demo.recipes.model.RecipeFilter;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface RecipeService {

    String createRecipe(RecipeRequest request);
    void updateRecipe(String id, RecipeRequest request);
    void deleteRecipe(String id);
    RecipeResponse getRecipeById(String id);
    Page<RecipeResponse> getRecipes(Pageable pageable, RecipeFilter recipeFilter);
}
