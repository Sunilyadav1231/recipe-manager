package com.demo.recipes.repository;

import com.demo.recipes.entity.Recipe;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, String> {

    List<Recipe> findByNameIgnoreCase(String name);

/*    @Query("SELECT r FROM   Recipes r INNER JOIN RecipeIngredients ri INNER JOIN Ingredient i " +
            "WHERE  r.instructions like :%text% " +
            " AND r.name = :name " +
            " AND r.type = :type " +
            " AND r.numberOfServing = :numberOfServing " +
            "HAVING COUNT(DISTINCT CASE WHEN i.code IN ( :ingredientInclude ) THEN i.code END) = 2 " +
            "AND MAX(CASE  WHEN i.code IN ( :ingredientExclude ) THEN 1  ELSE 0 END) = 0 ")
    Page<Recipe> getRecipesByFilters();*/

}
