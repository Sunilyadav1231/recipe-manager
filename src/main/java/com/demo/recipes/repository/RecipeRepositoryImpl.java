package com.demo.recipes.repository;

import com.demo.recipes.entity.Ingredient;
import com.demo.recipes.entity.Recipe;
import com.demo.recipes.entity.RecipeIngredients;
import com.demo.recipes.model.RecipeFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Repository
public class RecipeRepositoryImpl {

    @PersistenceContext
    private EntityManager entityManager;


    public static final String SEARCH_RECIPE_COUNT = "select count(distinct r.id) from recipe r  where r.retired = '0' ";
    public static final String SEARCH_RECIPE = "select distinct r.* from recipe r  where r.retired = '0' ";


    public Page<Recipe> getRecipes(RecipeFilter recipeFilter,Pageable pageable){
        String query = getRecipeQuery(SEARCH_RECIPE,recipeFilter);
        String queryCount = getRecipeQuery(SEARCH_RECIPE_COUNT,recipeFilter);
        return getRecipesData(pageable,query,queryCount);
    }

    private PageImpl<Recipe> getRecipesData(Pageable pageRequest, String queryString, String queryCountString) {
        Query queryTotal =entityManager.createNativeQuery(queryCountString);
        BigInteger countResult = (BigInteger) queryTotal.getSingleResult();
        Query query = entityManager.createNativeQuery(queryString,Recipe.class);
        query.setMaxResults(pageRequest.getPageSize());
        query.setFirstResult(pageRequest.getPageNumber() * pageRequest.getPageSize());
        List<Recipe> recipes = query.getResultList();
        return new PageImpl<>(recipes, pageRequest, countResult.longValue());
    }

    private String getRecipeQuery(String queryString, RecipeFilter recipeFilter){

        if (StringUtils.hasText(recipeFilter.getText())) {
            queryString+="AND (LOWER(r.instructions) LIKE '%"+recipeFilter.getText().toLowerCase()+"%') ";
        }
        if (recipeFilter.getType() != null) {
            queryString+="AND r.type='"+recipeFilter.getType()+"' ";
        }
        if (recipeFilter.getNumberOfServing() != null) {
           queryString+="AND r.number_of_serving="+recipeFilter.getNumberOfServing()+" ";
        }
        if (StringUtils.hasText(recipeFilter.getName())) {
            queryString+="AND (LOWER(r.name) LIKE '%"+recipeFilter.getName().toLowerCase()+"%') ";
        }
        if (StringUtils.hasText(recipeFilter.getIngredientInclude())) {
            String ingredient = recipeFilter.getIngredientInclude().trim().replace(" ", "_").toUpperCase(Locale.ROOT);
            queryString+="AND (SELECT COUNT(DISTINCT recipeingr1_.id) FROM recipe_ingredients recipeingr1_ WHERE ( recipeingr1_.retired = '0') AND recipeingr1_.ingredient_code='"+ingredient+"' AND r.id=recipeingr1_.recipe_id)>=1 ";
        }
        if (StringUtils.hasText(recipeFilter.getIngredientExclude())) {
            String ingredient = recipeFilter.getIngredientExclude().trim().replace(" ", "_").toUpperCase(Locale.ROOT);
            queryString+="AND (SELECT COUNT(DISTINCT recipeingr2_.id) FROM recipe_ingredients recipeingr2_ WHERE ( recipeingr2_.retired = '0') AND recipeingr2_.ingredient_code='"+ingredient+"' AND r.id=recipeingr2_.recipe_id)=0 ";
        }
        return queryString;
    }

    public Page<Recipe> filterRecipes(RecipeFilter recipeFilter, Pageable pageable) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Recipe> cq = cb.createQuery(Recipe.class);
        List<Recipe> result = getRecipes(recipeFilter, pageable, cb, cq);
        Long count = getRecipesCount(recipeFilter, cb, cq);

        return new PageImpl<>(result, pageable, count);
    }

    private List<Recipe> getRecipes(RecipeFilter recipeFilter, Pageable pageable, CriteriaBuilder cb, CriteriaQuery<Recipe> cq) {
        Root<Recipe> recipeRoot = cq.from(Recipe.class);
        Subquery<Long> subQuery = cq.subquery(Long.class);

        Root<RecipeIngredients> recipeIngredientsRoot = subQuery.from(RecipeIngredients.class);
        subQuery.select(cb.countDistinct(recipeIngredientsRoot));
        List<Predicate> predicates = getPredicates(recipeFilter, cb, recipeRoot, subQuery, recipeIngredientsRoot);

        new ArrayList<>();
        if (!CollectionUtils.isEmpty(predicates)) {
            cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        }
        cq.orderBy(cb.desc(recipeRoot.get("name")));
        cq.distinct(true);
        CriteriaQuery<Recipe> select = cq.select(recipeRoot);
        return entityManager.createQuery(select).setFirstResult((int) pageable.getOffset()).setMaxResults(pageable.getPageSize()).getResultList();
    }

    private Long getRecipesCount(RecipeFilter recipeFilter, CriteriaBuilder cb, CriteriaQuery<Recipe> cq) {
        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Recipe> recipeRootCount = countQuery.from(Recipe.class);
        Subquery<Long> subQueryCount = cq.subquery(Long.class);
        Root<RecipeIngredients> recipeIngredientsRootCount = subQueryCount.from(RecipeIngredients.class);
        List<Predicate> predicatesCount = getPredicates(recipeFilter, cb, recipeRootCount, subQueryCount, recipeIngredientsRootCount);
        countQuery.select(cb.countDistinct(recipeRootCount)).where(cb.and(predicatesCount.toArray(new Predicate[predicatesCount.size()])));
        return entityManager.createQuery(countQuery).getSingleResult();
    }

    private List<Predicate> getPredicates(RecipeFilter recipeFilter, CriteriaBuilder cb, Root<Recipe> recipeRoot, Subquery<Long> subQuery, Root<RecipeIngredients> recipeIngredientsRoot) {
        List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.hasText(recipeFilter.getText())) {
            predicates.add(cb.like(cb.lower(recipeRoot.get("instructions")),
                    "%" + recipeFilter.getText().toLowerCase() + "%"));
        }
        if (recipeFilter.getType() != null) {
            predicates.add(cb.equal(recipeRoot.get("type"), recipeFilter.getType()));
        }
        if (recipeFilter.getNumberOfServing() != null) {
            predicates.add(cb.equal(recipeRoot.get("numberOfServing"), recipeFilter.getNumberOfServing().toString()));
        }
        if (StringUtils.hasText(recipeFilter.getName())) {
            predicates.add(cb.equal(recipeRoot.get("name"), recipeFilter.getName()));
        }
        if (StringUtils.hasText(recipeFilter.getIngredientInclude())) {
            String ingredient = recipeFilter.getIngredientInclude().trim().replace(" ", "_").toUpperCase(Locale.ROOT);
            subQuery.select(cb.countDistinct(recipeIngredientsRoot)).where(cb.and(cb.equal(recipeIngredientsRoot.get("ingredient"),new Ingredient(ingredient)),
                    cb.equal(recipeRoot,recipeIngredientsRoot.get("recipe"))));
            predicates.add(cb.ge(subQuery.getSelection(), 1));
        }
        if (StringUtils.hasText(recipeFilter.getIngredientExclude())) {
            String ingredient = recipeFilter.getIngredientExclude().trim().replace(" ", "_").toUpperCase(Locale.ROOT);
            subQuery.select(cb.countDistinct(recipeIngredientsRoot)).where(cb.and(cb.equal(recipeIngredientsRoot.get("ingredient"), new Ingredient(ingredient)),
                    cb.equal(recipeRoot,recipeIngredientsRoot.get("recipe"))));
            predicates.add(cb.equal(subQuery.getSelection(), 0));
        }
        return predicates;
    }

}
