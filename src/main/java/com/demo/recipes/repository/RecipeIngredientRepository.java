package com.demo.recipes.repository;

import com.demo.recipes.entity.Recipe;
import com.demo.recipes.entity.RecipeIngredients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeIngredientRepository extends JpaRepository<RecipeIngredients,String> {

    List<RecipeIngredients> findByRecipe(Recipe  recipe);

    @Modifying
    void deleteByRecipe(Recipe recipe);
}
