package com.demo.recipes.resource;

import com.demo.recipes.model.RecipeFilter;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import com.demo.recipes.service.RecipeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/recipes")
public class RecipeEndpoint {
    private final RecipeService recipeService;

    public RecipeEndpoint(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @Operation(summary = "Get recipe by filters.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Recipe found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Page.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or invalid request data supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Recipe not found",
                    content = @Content)})
    @PostMapping
    public ResponseEntity<Page<RecipeResponse>> getAllRecipes(@RequestBody RecipeFilter filter, Pageable pageRequest) {
        return ResponseEntity.ok(recipeService.getRecipes(pageRequest, filter));
    }

    @Operation(summary = "Get recipe by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Recipe found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = RecipeResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id or invalid request data supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Recipe not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<RecipeResponse> getRecipeById(@PathVariable("id") @NotNull UUID id) {
        return ResponseEntity.ok(recipeService.getRecipeById(id.toString()));
    }

    @Operation(summary = "Create recipe by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Recipe successfully created",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Invalid id or invalid request data supplied",
                    content = @Content)})
    @PostMapping("/create")
    public ResponseEntity<Void> createRecipe(@RequestBody @Valid RecipeRequest request) {
        return ResponseEntity.created(URI.create(recipeService.createRecipe(request))).build();
    }

    @Operation(summary = "Update recipe by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Recipe successfully updated",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Invalid id or invalid request data supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Recipe not found",
                    content = @Content)})
    @PutMapping("/{id}")
    public ResponseEntity<Void> updateRecipe(@PathVariable("id") @NotNull UUID id, @RequestBody @Valid RecipeRequest request) {
        recipeService.updateRecipe(id.toString(), request);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Delete recipe by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Recipe successfully deleted",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Recipe not found",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRecipe(@PathVariable("id") @NotNull UUID id) {
        recipeService.deleteRecipe(id.toString());
        return ResponseEntity.noContent().build();
    }
}
