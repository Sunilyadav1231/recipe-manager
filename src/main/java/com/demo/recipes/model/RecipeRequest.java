package com.demo.recipes.model;

import com.demo.recipes.enums.RecipeType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class RecipeRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotEmpty
    private String name;
    @NotEmpty
    private String instructions;
    @NotNull
    private RecipeType type;
    @NotNull
    @Min(1)
    private Integer numberOfServing;
    @NotEmpty
    private List<String> ingredients;
}
