package com.demo.recipes.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String message;
    private Set<ErrorResponse> errors;

    public ErrorResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public void addFieldError(String path, String message) {
        ErrorResponse error = new ErrorResponse(path, message);
        if(CollectionUtils.isEmpty(this.errors)){
            this.errors=new HashSet<>();
        }
        this.errors.add(error);
    }
}
