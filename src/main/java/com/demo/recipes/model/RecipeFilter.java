package com.demo.recipes.model;

import com.demo.recipes.enums.RecipeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecipeFilter implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String text;
    private RecipeType type;
    private Integer numberOfServing;
    private String ingredientInclude;
    private String ingredientExclude;
}
