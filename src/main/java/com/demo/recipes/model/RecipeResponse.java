package com.demo.recipes.model;

import com.demo.recipes.enums.RecipeType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
public class RecipeResponse implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String instructions;
    private RecipeType type;
    private int numberOfServing;
    private List<String> ingredients;
}
