package com.demo.recipes.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.demo.recipes.entity.Ingredient;
import com.demo.recipes.entity.Recipe;
import com.demo.recipes.enums.RecipeType;
import com.demo.recipes.exception.ApiBusinessException;
import com.demo.recipes.mapper.RecipeMapper;
import com.demo.recipes.model.RecipeFilter;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import com.demo.recipes.repository.RecipeIngredientRepository;
import com.demo.recipes.repository.RecipeRepository;
import com.demo.recipes.repository.RecipeRepositoryImpl;
import com.demo.recipes.service.IngredientService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {RecipeServiceImpl.class})
@ExtendWith(SpringExtension.class)
class RecipeServiceImplTest {
    @MockBean
    private IngredientService ingredientService;

    @MockBean
    private RecipeIngredientRepository recipeIngredientRepository;

    @MockBean
    private RecipeMapper recipeMapper;

    @MockBean
    private RecipeRepository recipeRepository;

    @MockBean
    private RecipeRepositoryImpl recipeRepositoryImpl;

    @Autowired
    private RecipeServiceImpl recipeServiceImpl;

    /**
     * Method under test: {@link RecipeServiceImpl#createRecipe}
     */
    @Test
    void testCreateRecipe() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe);

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeMapper.getRecipeEntity( any())).thenReturn(recipe1);
        when(this.recipeIngredientRepository.saveAll( any())).thenReturn(new ArrayList<>());
        when(this.ingredientService.getIngredients( any())).thenReturn(new HashSet<>());
        assertEquals("42", this.recipeServiceImpl.createRecipe(new RecipeRequest()));
        verify(this.recipeRepository).save( any());
        verify(this.recipeMapper).getRecipeEntity( any());
        verify(this.recipeIngredientRepository).saveAll( any());
        verify(this.ingredientService).getIngredients( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#createRecipe}
     */
    @Test
    void testCreateRecipe2() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe);

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeMapper.getRecipeEntity( any())).thenReturn(recipe1);
        when(this.recipeIngredientRepository.saveAll( any())).thenReturn(new ArrayList<>());
        when(this.ingredientService.getIngredients( any()))
                .thenThrow(new ApiBusinessException(HttpStatus.CONTINUE, "An error occurred", "Msg"));
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.createRecipe(new RecipeRequest()));
        verify(this.recipeRepository).save( any());
        verify(this.recipeMapper).getRecipeEntity( any());
        verify(this.ingredientService).getIngredients( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#createRecipe}
     */
    @Test
    void testCreateRecipe3() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe);

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeMapper.getRecipeEntity( any())).thenReturn(recipe1);
        when(this.recipeIngredientRepository.saveAll( any())).thenReturn(new ArrayList<>());

        Ingredient ingredient = new Ingredient();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setCreatedAt(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        ingredient.setId("42");
        ingredient.setName("Name");
        ingredient.setRetired(true);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setUpdatedAt(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        HashSet<Ingredient> ingredientSet = new HashSet<>();
        ingredientSet.add(ingredient);
        when(this.ingredientService.getIngredients( any())).thenReturn(ingredientSet);
        assertEquals("42", this.recipeServiceImpl.createRecipe(new RecipeRequest()));
        verify(this.recipeRepository).save( any());
        verify(this.recipeMapper).getRecipeEntity( any());
        verify(this.recipeIngredientRepository).saveAll( any());
        verify(this.ingredientService).getIngredients( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#updateRecipe(String, RecipeRequest)}
     */
    @Test
    void testUpdateRecipe() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<Recipe> ofResult = Optional.of(recipe);

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe1);
        when(this.recipeRepository.findById( any())).thenReturn(ofResult);
        when(this.recipeMapper.mapRecipeIngredientsEntity( any(),  any()))
                .thenReturn(new HashSet<>());
        doNothing().when(this.recipeMapper).mapRecipeEntity( any(),  any());
        when(this.recipeIngredientRepository.saveAll( any())).thenReturn(new ArrayList<>());
        doNothing().when(this.recipeIngredientRepository).deleteByRecipe( any());
        when(this.ingredientService.getIngredients( any())).thenReturn(new HashSet<>());
        this.recipeServiceImpl.updateRecipe("42", new RecipeRequest());
        verify(this.recipeRepository).save( any());
        verify(this.recipeRepository).findById( any());
        verify(this.recipeMapper).mapRecipeIngredientsEntity( any(),  any());
        verify(this.recipeMapper).mapRecipeEntity( any(),  any());
        verify(this.recipeIngredientRepository).saveAll( any());
        verify(this.recipeIngredientRepository).deleteByRecipe( any());
        verify(this.ingredientService).getIngredients( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#updateRecipe(String, RecipeRequest)}
     */
    @Test
    void testUpdateRecipe2() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<Recipe> ofResult = Optional.of(recipe);

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe1);
        when(this.recipeRepository.findById( any())).thenReturn(ofResult);
        when(this.recipeMapper.mapRecipeIngredientsEntity( any(),  any()))
                .thenReturn(new HashSet<>());
        doNothing().when(this.recipeMapper).mapRecipeEntity( any(),  any());
        when(this.recipeIngredientRepository.saveAll( any())).thenReturn(new ArrayList<>());
        doNothing().when(this.recipeIngredientRepository).deleteByRecipe( any());
        when(this.ingredientService.getIngredients( any()))
                .thenThrow(new ApiBusinessException(HttpStatus.CONTINUE, "An error occurred", "Msg"));
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.updateRecipe("42", new RecipeRequest()));
        verify(this.recipeRepository).findById( any());
        verify(this.ingredientService).getIngredients( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#updateRecipe(String, RecipeRequest)}
     */
    @Test
    void testUpdateRecipe3() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe);
        when(this.recipeRepository.findById( any())).thenReturn(Optional.empty());
        when(this.recipeMapper.mapRecipeIngredientsEntity( any(),  any()))
                .thenReturn(new HashSet<>());
        doNothing().when(this.recipeMapper).mapRecipeEntity( any(),  any());
        when(this.recipeIngredientRepository.saveAll( any())).thenReturn(new ArrayList<>());
        doNothing().when(this.recipeIngredientRepository).deleteByRecipe( any());
        when(this.ingredientService.getIngredients( any())).thenReturn(new HashSet<>());
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.updateRecipe("42", new RecipeRequest()));
        verify(this.recipeRepository).findById( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#deleteRecipe}
     */
    @Test
    void testDeleteRecipe() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<Recipe> ofResult = Optional.of(recipe);

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe1);
        when(this.recipeRepository.findById( any())).thenReturn(ofResult);
        this.recipeServiceImpl.deleteRecipe("42");
        verify(this.recipeRepository).save( any());
        verify(this.recipeRepository).findById( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#deleteRecipe}
     */
    @Test
    void testDeleteRecipe2() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<Recipe> ofResult = Optional.of(recipe);
        when(this.recipeRepository.save( any()))
                .thenThrow(new ApiBusinessException(HttpStatus.CONTINUE, "An error occurred", "Msg"));
        when(this.recipeRepository.findById( any())).thenReturn(ofResult);
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.deleteRecipe("42"));
        verify(this.recipeRepository).save( any());
        verify(this.recipeRepository).findById( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#deleteRecipe}
     */
    @Test
    void testDeleteRecipe3() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.recipeRepository.save( any())).thenReturn(recipe);
        when(this.recipeRepository.findById( any())).thenReturn(Optional.empty());
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.deleteRecipe("42"));
        verify(this.recipeRepository).findById( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#getRecipeById}
     */
    @Test
    void testGetRecipeById() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<Recipe> ofResult = Optional.of(recipe);
        when(this.recipeRepository.findById( any())).thenReturn(ofResult);
        RecipeResponse recipeResponse = new RecipeResponse();
        when(this.recipeMapper.getRecipeResponse( any())).thenReturn(recipeResponse);
        assertSame(recipeResponse, this.recipeServiceImpl.getRecipeById("42"));
        verify(this.recipeRepository).findById( any());
        verify(this.recipeMapper).getRecipeResponse( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#getRecipeById}
     */
    @Test
    void testGetRecipeById2() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<Recipe> ofResult = Optional.of(recipe);
        when(this.recipeRepository.findById( any())).thenReturn(ofResult);
        when(this.recipeMapper.getRecipeResponse( any()))
                .thenThrow(new ApiBusinessException(HttpStatus.CONTINUE, "An error occurred", "Msg"));
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.getRecipeById("42"));
        verify(this.recipeRepository).findById( any());
        verify(this.recipeMapper).getRecipeResponse( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#getRecipeById}
     */
    @Test
    void testGetRecipeById3() {
        when(this.recipeRepository.findById( any())).thenReturn(Optional.empty());
        when(this.recipeMapper.getRecipeResponse( any())).thenReturn(new RecipeResponse());
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.getRecipeById("42"));
        verify(this.recipeRepository).findById( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#getRecipes(org.springframework.data.domain.Pageable, RecipeFilter)}
     */
    @Test
    void testGetRecipes() {
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.getRecipes(null, new RecipeFilter()));
    }

    /**
     * Method under test: {@link RecipeServiceImpl#getRecipe}
     */
    @Test
    void testGetRecipe() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<Recipe> ofResult = Optional.of(recipe);
        when(this.recipeRepository.findById( any())).thenReturn(ofResult);
        assertSame(recipe, this.recipeServiceImpl.getRecipe("42"));
        verify(this.recipeRepository).findById( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#getRecipe}
     */
    @Test
    void testGetRecipe2() {
        when(this.recipeRepository.findById( any())).thenReturn(Optional.empty());
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.getRecipe("42"));
        verify(this.recipeRepository).findById( any());
    }

    /**
     * Method under test: {@link RecipeServiceImpl#getRecipe}
     */
    @Test
    void testGetRecipe3() {
        when(this.recipeRepository.findById( any()))
                .thenThrow(new ApiBusinessException(HttpStatus.CONTINUE, "An error occurred", "4004"));
        assertThrows(ApiBusinessException.class, () -> this.recipeServiceImpl.getRecipe("42"));
        verify(this.recipeRepository).findById( any());
    }
}

