package com.demo.recipes.service.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.demo.recipes.entity.Ingredient;
import com.demo.recipes.exception.ApiBusinessException;
import com.demo.recipes.repository.IngredientRepository;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {IngredientServiceImpl.class})
@ExtendWith(SpringExtension.class)
class IngredientServiceImplTest {
    @MockBean
    private IngredientRepository ingredientRepository;

    @Autowired
    private IngredientServiceImpl ingredientServiceImpl;

    /**
     * Method under test: {@link IngredientServiceImpl#getIngredients(java.util.List)}
     */
    @Test
    void testGetIngredients() {
        assertThrows(ApiBusinessException.class, () -> this.ingredientServiceImpl.getIngredients(new ArrayList<>()));
    }

    /**
     * Method under test: {@link IngredientServiceImpl#getIngredients(List)}
     */
    @Test
    void testGetIngredients2() {
        when(this.ingredientRepository.saveAll( any())).thenReturn(new ArrayList<>());

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("4010");
        assertTrue(this.ingredientServiceImpl.getIngredients(stringList).isEmpty());
        verify(this.ingredientRepository).saveAll( any());
    }

    /**
     * Method under test: {@link IngredientServiceImpl#getIngredients(List)}
     */
    @Test
    void testGetIngredients3() {
        when(this.ingredientRepository.saveAll( any())).thenReturn(new ArrayList<>());

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add(" ");
        stringList.add("4010");
        assertTrue(this.ingredientServiceImpl.getIngredients(stringList).isEmpty());
        verify(this.ingredientRepository).saveAll( any());
    }

    /**
     * Method under test: {@link IngredientServiceImpl#getIngredients(List)}
     */
    @Test
    void testGetIngredients4() {
        when(this.ingredientRepository.saveAll( any()))
                .thenThrow(new ApiBusinessException(HttpStatus.CONTINUE, "An error occurred", " "));

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("4010");
        assertThrows(ApiBusinessException.class, () -> this.ingredientServiceImpl.getIngredients(stringList));
        verify(this.ingredientRepository).saveAll( any());
    }
}

