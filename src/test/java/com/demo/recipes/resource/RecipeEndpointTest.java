package com.demo.recipes.resource;

import com.demo.recipes.enums.RecipeType;
import com.demo.recipes.model.RecipeFilter;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import com.demo.recipes.service.RecipeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {RecipeEndpoint.class})
@ExtendWith(SpringExtension.class)
class RecipeEndpointTest {
    @Autowired
    private RecipeEndpoint recipeEndpoint;

    @MockBean
    private RecipeService recipeService;

    /**
     * Method under test: {@link RecipeEndpoint#getAllRecipes(RecipeFilter, org.springframework.data.domain.Pageable)}
     */
    @Test
    void testGetAllRecipes() throws Exception {
        RecipeFilter recipeFilter = new RecipeFilter();
        recipeFilter.setIngredientExclude("Ingredient Exclude");
        recipeFilter.setIngredientInclude("Ingredient Include");
        recipeFilter.setName("Name");
        recipeFilter.setNumberOfServing(10);
        recipeFilter.setText("Text");
        recipeFilter.setType(RecipeType.VEGETARIAN);
        String content = (new ObjectMapper()).writeValueAsString(recipeFilter);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/recipes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(405));
    }

    /**
     * Method under test: {@link RecipeEndpoint#getRecipeById(UUID)}
     */
    @Test
    void testGetRecipeById() throws Exception {
        when(this.recipeService.getRecipeById((String) any())).thenReturn(new RecipeResponse());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/recipes/{id}",
                UUID.randomUUID());
        MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("{\"numberOfServing\":0}"));
    }

    /**
     * Method under test: {@link RecipeEndpoint#getRecipeById(UUID)}
     */
    @Test
    void testGetRecipeById2() throws Exception {
        when(this.recipeService.getRecipeById(any())).thenReturn(new RecipeResponse());
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/recipes/{id}", UUID.randomUUID());
        getResult.contentType("https://example.org/example");
        MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.content().string("{\"numberOfServing\":0}"));
    }

    /**
     * Method under test: {@link RecipeEndpoint#createRecipe(RecipeRequest)}
     */
    @Test
    void testCreateRecipe() throws Exception {
        RecipeRequest recipeRequest = new RecipeRequest();
        recipeRequest.setIngredients(new ArrayList<>());
        recipeRequest.setInstructions("Instructions");
        recipeRequest.setName("Name");
        recipeRequest.setNumberOfServing(10);
        recipeRequest.setType(RecipeType.VEGETARIAN);
        String content = (new ObjectMapper()).writeValueAsString(recipeRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/recipes/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }

    /**
     * Method under test: {@link RecipeEndpoint#createRecipe(RecipeRequest)}
     */
    @Test
    void testCreateRecipe2() throws Exception {
        when(this.recipeService.createRecipe((RecipeRequest) any())).thenReturn("?");

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("?");

        RecipeRequest recipeRequest = new RecipeRequest();
        recipeRequest.setIngredients(stringList);
        recipeRequest.setInstructions("Instructions");
        recipeRequest.setName("Name");
        recipeRequest.setNumberOfServing(10);
        recipeRequest.setType(RecipeType.VEGETARIAN);
        String content = (new ObjectMapper()).writeValueAsString(recipeRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/recipes/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.redirectedUrl("?"));
    }


    /**
     * Method under test: {@link RecipeEndpoint#updateRecipe(UUID, RecipeRequest)}
     */
    @Test
    void testUpdateRecipe2() throws Exception {
        doNothing().when(this.recipeService).updateRecipe((String) any(), (RecipeRequest) any());

        ArrayList<String> stringList = new ArrayList<>();
        stringList.add("?");

        RecipeRequest recipeRequest = new RecipeRequest();
        recipeRequest.setIngredients(stringList);
        recipeRequest.setInstructions("Instructions");
        recipeRequest.setName("Name");
        recipeRequest.setNumberOfServing(10);
        recipeRequest.setType(RecipeType.VEGETARIAN);
        String content = (new ObjectMapper()).writeValueAsString(recipeRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/v1/recipes/{id}", UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Method under test: {@link RecipeEndpoint#deleteRecipe(UUID)}
     */
    @Test
    void testDeleteRecipe() throws Exception {
        doNothing().when(this.recipeService).deleteRecipe((String) any());
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/recipes/{id}",
                UUID.randomUUID());
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Method under test: {@link RecipeEndpoint#deleteRecipe(UUID)}
     */
    @Test
    void testDeleteRecipe2() throws Exception {
        doNothing().when(this.recipeService).deleteRecipe((String) any());
        MockHttpServletRequestBuilder deleteResult = MockMvcRequestBuilders.delete("/api/v1/recipes/{id}",
                UUID.randomUUID());
        deleteResult.contentType("https://example.org/example");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(deleteResult);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    /**
     * Method under test: {@link RecipeEndpoint#updateRecipe(UUID, RecipeRequest)}
     */
    @Test
    void testUpdateRecipe() throws Exception {
        RecipeRequest recipeRequest = new RecipeRequest();
        recipeRequest.setIngredients(new ArrayList<>());
        recipeRequest.setInstructions("Instructions");
        recipeRequest.setName("Name");
        recipeRequest.setNumberOfServing(10);
        recipeRequest.setType(RecipeType.VEGETARIAN);
        String content = (new ObjectMapper()).writeValueAsString(recipeRequest);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/v1/recipes/{id}", UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(this.recipeEndpoint)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(400));
    }
}

