package com.demo.recipes.resource;

import com.demo.recipes.enums.RecipeType;
import com.demo.recipes.model.RecipeFilter;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RecipeEndpointIntegrationTest {


    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate template;

    @Autowired
    private ObjectMapper objectMapper;


    private final static String BASE_URI = "http://localhost";
    private static final String RECIPE_ENDPOINT_URL = "/api/v1/recipes";
    private static final String RECIPE_NAME = "Recipe Name";
    private static final String RECIPE_INSTRUCTION = "Recipe Instruction";
    private static final RecipeType RECIPE_TYPE = RecipeType.VEGAN;
    private static final Integer RECIPE_SERVE_COUNT = 2;
    private static final String INGREDIENTS = "Salt";


    @BeforeEach
    public void configureRestAssured() {
        RestAssured.baseURI = BASE_URI;
        RestAssured.port = port;
    }

    @Test
    void getRecipeByIdTest() throws JsonProcessingException {
        String id = createRecipe();
        JsonPath expectedJson = new JsonPath(objectMapper.writeValueAsString(getRecipeResponse(id)));
        given().pathParam("id", id).get(RECIPE_ENDPOINT_URL + "/{id}")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body("", equalTo(expectedJson.getMap("")));
    }

    @Test
    void getRecipesWithFilterTest() throws JsonProcessingException {

        createRecipe(getRecipeRequest("name1", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 2));
        createRecipe(getRecipeRequest("name2", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 4));
        createRecipe(getRecipeRequest("name3", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 6));
        createRecipe(getRecipeRequest("name4", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 8));
        createRecipe(getRecipeRequest("name5", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 10));
        createRecipe(getRecipeRequest("name6", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 12));
        String id7 = createRecipe(getRecipeRequest("name7", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 14));

        List<RecipeResponse> recipeResponses = new ArrayList<>();
        recipeResponses.add(getRecipeResponse(id7, "name7", "instruction1", RecipeType.VEGAN, Arrays.asList("Salt"), 14));

        JsonPath expectedJson = new JsonPath(objectMapper.writeValueAsString(recipeResponses));
        RecipeFilter recipeFilter = RecipeFilter.builder().name("name7").ingredientInclude("Salt").build();
        given().contentType(ContentType.JSON)
                .body(recipeFilter)
                .post(RECIPE_ENDPOINT_URL)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body("content", equalTo(expectedJson.get("")));
        //.body("content",  Matchers.hasItem(equalTo(expectedJson.get(""))));
    }

    @Test
    void getRecipesWithIngredientFilterTest() throws JsonProcessingException {

        String id1 = createRecipe(getRecipeRequest("name1", "instruction1", RecipeType.VEGETARIAN, Collections.singletonList("Salt"), 2));
        createRecipe(getRecipeRequest("name2", "instruction1", RecipeType.VEGETARIAN, Collections.singletonList("Sugar"), 4));
        createRecipe(getRecipeRequest("name2", "instruction1", RecipeType.VEGETARIAN, Arrays.asList("Sugar","Salt", "Oil"), 4));

        List<RecipeResponse> recipeResponses = new ArrayList<>();
        recipeResponses.add(getRecipeResponse(id1, "name1", "instruction1", RecipeType.VEGETARIAN, Collections.singletonList("Salt"), 2));

        JsonPath expectedJson = new JsonPath(objectMapper.writeValueAsString(recipeResponses));
        RecipeFilter recipeFilter = RecipeFilter.builder().type(RecipeType.VEGETARIAN).ingredientInclude("Salt").ingredientExclude("Oil").build();
        given().contentType(ContentType.JSON)
                .body(recipeFilter)
                .post(RECIPE_ENDPOINT_URL)
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body("content", equalTo(expectedJson.get("")));
        //.body("content",  Matchers.hasItem(equalTo(expectedJson.get(""))));
    }


    @Test
    void createRecipeTest() {
        given().contentType(ContentType.JSON)
                .body(getRecipeRequest()).post(RECIPE_ENDPOINT_URL + "/create")
                .then()
                .statusCode(201)
                .header("location", notNullValue());
    }

    @Test
    void updateRecipeTest() throws JsonProcessingException {
        String id = createRecipe();
        RecipeRequest request = getRecipeRequest();
        request.setInstructions("Change Instruction");
        request.setIngredients(Collections.singletonList("Sugar"));

        given().contentType(ContentType.JSON)
                .pathParam("id", id)
                .body(request).put(RECIPE_ENDPOINT_URL + "/{id}")
                .then()
                .statusCode(200);
        RecipeResponse expectedResponse = getRecipeResponse(id);
        expectedResponse.setInstructions("Change Instruction");
        expectedResponse.setIngredients(Collections.singletonList("Sugar"));
        JsonPath expectedJson = new JsonPath(objectMapper.writeValueAsString(expectedResponse));
        given().pathParam("id", id).get(RECIPE_ENDPOINT_URL + "/{id}")
                .then()
                .body("", equalTo(expectedJson.getMap("")));

    }

    @Test
    void deleteRecipeByIdTest() {
        String id = createRecipe();
        given().pathParam("id", id).delete(RECIPE_ENDPOINT_URL + "/{id}")
                .then()
                .statusCode(204);

        given().pathParam("id", id).get(RECIPE_ENDPOINT_URL + "/{id}")
                .then()
                .statusCode(404);
    }

    @Test
    void deleteRecipeByIdResourceNotExistTest() {
        given().pathParam("id", UUID.randomUUID().toString()).delete(RECIPE_ENDPOINT_URL + "/{id}")
                .then()
                .statusCode(404);
    }

    private String createRecipe() {
        HttpEntity<RecipeRequest> request = new HttpEntity<>(getRecipeRequest());
        ResponseEntity<RecipeResponse> response = template.postForEntity(RECIPE_ENDPOINT_URL + "/create", request, RecipeResponse.class);
        return Objects.requireNonNull(response.getHeaders().get("location")).get(0);
    }

    private String createRecipe(RecipeRequest recipeRequest) {
        HttpEntity<RecipeRequest> request = new HttpEntity<>(recipeRequest);
        ResponseEntity<RecipeResponse> response = template.postForEntity(RECIPE_ENDPOINT_URL + "/create", request, RecipeResponse.class);
        return Objects.requireNonNull(response.getHeaders().get("location")).get(0);
    }

    private RecipeResponse getRecipeResponse(String id) {
        return RecipeResponse.builder()
                .id(id)
                .instructions(RECIPE_INSTRUCTION)
                .ingredients(Collections.singletonList(INGREDIENTS))
                .name(RECIPE_NAME)
                .type(RECIPE_TYPE)
                .numberOfServing(RECIPE_SERVE_COUNT)
                .build();
    }

    private RecipeResponse getRecipeResponse(String id, String name, String instruction, RecipeType type, List<String> ingredients, Integer serveCount) {
        return RecipeResponse.builder()
                .id(id)
                .instructions(instruction)
                .ingredients(ingredients)
                .name(name)
                .type(type)
                .numberOfServing(serveCount)
                .build();
    }

    private RecipeRequest getRecipeRequest(String name, String instruction, RecipeType type, List<String> ingredients, Integer serveCount) {
        return RecipeRequest.builder().name(name)
                .instructions(instruction)
                .type(type)
                .numberOfServing(serveCount)
                .ingredients(ingredients)
                .build();
    }

    private RecipeRequest getRecipeRequest() {
        return RecipeRequest.builder().name(RECIPE_NAME)
                .instructions(RECIPE_INSTRUCTION)
                .type(RECIPE_TYPE)
                .numberOfServing(RECIPE_SERVE_COUNT)
                .ingredients(Collections.singletonList(INGREDIENTS))
                .build();
    }
}
