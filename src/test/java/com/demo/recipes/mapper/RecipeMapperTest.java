package com.demo.recipes.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyBoolean;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.demo.recipes.entity.Ingredient;
import com.demo.recipes.entity.Recipe;
import com.demo.recipes.entity.RecipeIngredients;
import com.demo.recipes.enums.RecipeType;
import com.demo.recipes.model.RecipeRequest;
import com.demo.recipes.model.RecipeResponse;
import com.demo.recipes.repository.RecipeIngredientRepository;
import com.demo.recipes.service.IngredientService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {RecipeMapper.class})
@ExtendWith(SpringExtension.class)
class RecipeMapperTest {
    @MockBean
    private IngredientService ingredientService;

    @MockBean
    private ModelMapper modelMapper;

    @MockBean
    private RecipeIngredientRepository recipeIngredientRepository;

    @Autowired
    private RecipeMapper recipeMapper;

    /**
     * Method under test: {@link RecipeMapper#getRecipeEntity(RecipeRequest)}
     */
    @Test
    void testGetRecipeEntity() {
        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(this.modelMapper.map( any(), (Class<Recipe>) any())).thenReturn(recipe);
        assertSame(recipe, this.recipeMapper.getRecipeEntity(new RecipeRequest()));
        verify(this.modelMapper).map( any(), (Class<Recipe>) any());
    }

    /**
     * Method under test: {@link RecipeMapper#mapRecipeIngredientsEntity(Set, Recipe)}
     */
    @Test
    void testMapRecipeIngredientsEntity() {
        HashSet<Ingredient> ingredients = new HashSet<>();

        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        assertTrue(this.recipeMapper.mapRecipeIngredientsEntity(ingredients, recipe).isEmpty());
    }

    /**
     * Method under test: {@link RecipeMapper#mapRecipeIngredientsEntity(Set, Recipe)}
     */
    @Test
    void testMapRecipeIngredientsEntity2() {
        Ingredient ingredient = new Ingredient();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        ingredient.setId("42");
        ingredient.setName("Name");
        ingredient.setRetired(true);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        HashSet<Ingredient> ingredientSet = new HashSet<>();
        ingredientSet.add(ingredient);

        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        assertEquals(1, this.recipeMapper.mapRecipeIngredientsEntity(ingredientSet, recipe).size());
    }

    /**
     * Method under test: {@link RecipeMapper#mapRecipeIngredientsEntity(Set, Recipe)}
     */
    @Test
    void testMapRecipeIngredientsEntity3() {
        Ingredient ingredient = new Ingredient();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        ingredient.setId("42");
        ingredient.setName("Name");
        ingredient.setRetired(true);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        Ingredient ingredient1 = new Ingredient();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient1.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        ingredient1.setId("42");
        ingredient1.setName("Name");
        ingredient1.setRetired(true);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient1.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        HashSet<Ingredient> ingredientSet = new HashSet<>();
        ingredientSet.add(ingredient1);
        ingredientSet.add(ingredient);

        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        assertEquals(2, this.recipeMapper.mapRecipeIngredientsEntity(ingredientSet, recipe).size());
    }

    /**
     * Method under test: {@link RecipeMapper#mapRecipeIngredientsEntity(Set, Recipe)}
     */
    @Test
    void testMapRecipeIngredientsEntity4() {
        HashSet<Ingredient> ingredients = new HashSet<>();
        Recipe recipe = mock(Recipe.class);
        doNothing().when(recipe).setCreatedAt( any());
        doNothing().when(recipe).setId( any());
        doNothing().when(recipe).setRetired(anyBoolean());
        doNothing().when(recipe).setUpdatedAt( any());
        doNothing().when(recipe).setInstructions( any());
        doNothing().when(recipe).setName( any());
        doNothing().when(recipe).setNumberOfServing( any());
        doNothing().when(recipe).setType( any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        assertTrue(this.recipeMapper.mapRecipeIngredientsEntity(ingredients, recipe).isEmpty());
        verify(recipe).setCreatedAt( any());
        verify(recipe).setId( any());
        verify(recipe).setRetired(anyBoolean());
        verify(recipe).setUpdatedAt( any());
        verify(recipe).setInstructions( any());
        verify(recipe).setName( any());
        verify(recipe).setNumberOfServing( any());
        verify(recipe).setType( any());
    }

    /**
     * Method under test: {@link RecipeMapper#getRecipeResponse}
     */
    @Test
    void testGetRecipeResponse() {
        when(this.recipeIngredientRepository.findByRecipe( any())).thenReturn(new ArrayList<>());
        RecipeResponse recipeResponse = new RecipeResponse();
        when(this.modelMapper.map( any(),  any())).thenReturn(recipeResponse);

        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        assertSame(recipeResponse, this.recipeMapper.getRecipeResponse(recipe));
        verify(this.recipeIngredientRepository).findByRecipe( any());
        verify(this.modelMapper).map( any(),  any());
    }

    /**
     * Method under test: {@link RecipeMapper#getRecipeResponse}
     */
    @Test
    void testGetRecipeResponse2() {
        Ingredient ingredient = new Ingredient();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        ingredient.setId("42");
        ingredient.setName("Name");
        ingredient.setRetired(true);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        RecipeIngredients recipeIngredients = new RecipeIngredients();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipeIngredients.setCreatedAt(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        recipeIngredients.setId("42");
        recipeIngredients.setIngredient(ingredient);
        recipeIngredients.setRecipe(recipe);
        recipeIngredients.setRetired(true);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipeIngredients.setUpdatedAt(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        ArrayList<RecipeIngredients> recipeIngredientsList = new ArrayList<>();
        recipeIngredientsList.add(recipeIngredients);
        when(this.recipeIngredientRepository.findByRecipe( any())).thenReturn(recipeIngredientsList);
        RecipeResponse recipeResponse = new RecipeResponse();
        when(this.modelMapper.map( any(),  any())).thenReturn(recipeResponse);

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        RecipeResponse actualRecipeResponse = this.recipeMapper.getRecipeResponse(recipe1);
        assertSame(recipeResponse, actualRecipeResponse);
        assertEquals(1, actualRecipeResponse.getIngredients().size());
        verify(this.recipeIngredientRepository).findByRecipe( any());
        verify(this.modelMapper).map( any(),  any());
    }

    /**
     * Method under test: {@link RecipeMapper#getRecipeResponse}
     */
    @Test
    void testGetRecipeResponse3() {
        Ingredient ingredient = new Ingredient();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setCreatedAt(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        ingredient.setId("42");
        ingredient.setName("Name");
        ingredient.setRetired(true);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient.setUpdatedAt(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        Recipe recipe = new Recipe();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setCreatedAt(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        recipe.setId("42");
        recipe.setInstructions("Instructions");
        recipe.setName("Name");
        recipe.setNumberOfServing(10);
        recipe.setRetired(true);
        recipe.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe.setUpdatedAt(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        RecipeIngredients recipeIngredients = new RecipeIngredients();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipeIngredients.setCreatedAt(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        recipeIngredients.setId("42");
        recipeIngredients.setIngredient(ingredient);
        recipeIngredients.setRecipe(recipe);
        recipeIngredients.setRetired(true);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipeIngredients.setUpdatedAt(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        Ingredient ingredient1 = new Ingredient();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient1.setCreatedAt(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        ingredient1.setId("42");
        ingredient1.setName("Name");
        ingredient1.setRetired(true);
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        ingredient1.setUpdatedAt(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        Recipe recipe1 = new Recipe();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setCreatedAt(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        recipe1.setId("42");
        recipe1.setInstructions("Instructions");
        recipe1.setName("Name");
        recipe1.setNumberOfServing(10);
        recipe1.setRetired(true);
        recipe1.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe1.setUpdatedAt(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        RecipeIngredients recipeIngredients1 = new RecipeIngredients();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipeIngredients1.setCreatedAt(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        recipeIngredients1.setId("42");
        recipeIngredients1.setIngredient(ingredient1);
        recipeIngredients1.setRecipe(recipe1);
        recipeIngredients1.setRetired(true);
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipeIngredients1.setUpdatedAt(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));

        ArrayList<RecipeIngredients> recipeIngredientsList = new ArrayList<>();
        recipeIngredientsList.add(recipeIngredients1);
        recipeIngredientsList.add(recipeIngredients);
        when(this.recipeIngredientRepository.findByRecipe( any())).thenReturn(recipeIngredientsList);
        RecipeResponse recipeResponse = new RecipeResponse();
        when(this.modelMapper.map( any(),  any())).thenReturn(recipeResponse);

        Recipe recipe2 = new Recipe();
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe2.setCreatedAt(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        recipe2.setId("42");
        recipe2.setInstructions("Instructions");
        recipe2.setName("Name");
        recipe2.setNumberOfServing(10);
        recipe2.setRetired(true);
        recipe2.setType(RecipeType.VEGETARIAN);
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        recipe2.setUpdatedAt(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        RecipeResponse actualRecipeResponse = this.recipeMapper.getRecipeResponse(recipe2);
        assertSame(recipeResponse, actualRecipeResponse);
        assertEquals(2, actualRecipeResponse.getIngredients().size());
        verify(this.recipeIngredientRepository).findByRecipe( any());
        verify(this.modelMapper).map( any(),  any());
    }
}

