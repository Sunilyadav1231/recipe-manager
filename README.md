# Recipe Manager

- [Recipe Manager](#recipe-manager)
    * [Objective](#objective)
    * [Quick Start](#quick-start)
    * [Explore Specification](#explore-specification)
    * [Tech Specification](#tech-specification)
    * [Database Schema](#database-schema)
    * [Rest API](#rest-api)
        - [Get Recipe by its id](###Get recipe by its id)
        - [Get recipes by filter](###Get recipes by filter)
        - [Create recipe](###Create recipe)
        - [Update recipe](###Update recipe)
        - [Delete recipe](###Delete recipe) 

## Objective
This standalone application which allows users to manage their favourite recipes. It should allow adding, updating, removing and fetching recipes. Additionally users should be able to filter available recipes based on one or more of the following criteria:  
1.  Whether the dish is vegetarian 
2. The number of servings 
3. Specific ingredients (either include or exclude) 
4. Text search within the instructions. 

For example, the API should be able to handle the following search requests:  
•  All vegetarian recipes  
•  Recipes that can serve 4 persons and have “potatoes” as an ingredient  
•  Recipes without “salmon” as an ingredient that has “oven” in the instructions.




##Prerequisite
 - Java 8
 - Maven
 - MySql

## Quick Start

Run via Docker:

    docker-compose build
 then

    docker-compose up


**Note**: Docker file might have some issue as I am facing some issue. You can run alternatively


####Alternatively

Change the mysql credentials in 

   **recipe-manager\src\main\resources\application.yaml**



    mvn  spring-boot:run
    
To run test cases:

    mvn  test



## Explore Specification

- [OpenAPI 3.0 Specification](http://localhost:8080/v3/api-docs)
   
- [Swagger UI](http://localhost:8080/swagger-ui/index.html)
   


## Tech Specification

 - **Language**: Java 8
 - **Framework**: SpringBoot 2.7.1
 - **Database**: H2 (In memory)

## Database Schema

![img_1.png](img_1.png)

## Rest API

###Get recipe by its id

`GET /api/v1/recipes/{id}`

#### Request

    curl -X 'GET' \
    'http://localhost:8080/api/v1/recipes/ad15751d-440a-413b-9006-832179232018' \
    -H 'accept: application/json'

#### Response [200]

    **RESPONSE HEADER**
    connection: keep-alive
    content-type: application/json
    date: Sat,27 Aug 2022 09:30:20 GMT
    keep-alive: timeout=60
    transfer-encoding: chunked

    **RESPONSE BODY**
    {
        "id": "ad15751d-440a-413b-9006-832179232018",
        "name": "string",
        "instructions": "string",
        "type": "VEGETARIAN",
        "numberOfServing": 1,
        "ingredients": [
        "string"
        ]
    }


###Get recipes by filter

`POST /api/v1/recipes`


#### Request

    curl -X 'POST' \
    'http://localhost:8080/api/v1/recipes?page=0&size=1&sort=string' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{
    "name": "string",
    "text": "string",
    "type": "VEGETARIAN",
    "numberOfServing": 0,
    "ingredientInclude": "string",
    "ingredientExclude": "string"
    }'


#### Response [200]

    **RESPONSE HEADER**
    connection: keep-alive
    content-type: application/json
    date: Sat,27 Aug 2022 09:52:21 GMT
    keep-alive: timeout=60
    transfer-encoding: chunked

    **RESPONSE BODY**
    {
        "content": [
        {
            "id": "6a8aefd8-8526-4edd-827c-2e1a3c6123f5",
            "name": "string",
            "instructions": "string",
            "type": "VEGETARIAN",
            "numberOfServing": 1,
            "ingredients": [
            "string"
            ]
        }
        ],
        "pageable": {
            "sort": {
                "empty": false,
                "unsorted": false,
                "sorted": true
            },
            "offset": 0,
            "pageSize": 1,
            "pageNumber": 0,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 5,
        "totalPages": 5,
        "last": false,
        "size": 1,
        "number": 0,
        "sort": {
            "empty": false,
            "unsorted": false,
            "sorted": true
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }

###Create recipe

`POST /api/v1/recipes/create`


#### Request

    curl -X 'POST' \
    'http://localhost:8080/api/v1/recipes/create' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{
    "name": "string",
    "instructions": "string",
    "type": "VEGETARIAN",
    "numberOfServing": 1,
    "ingredients": [
    "string"
    ]


#### Response [201]

    **RESPONSE HEADER**
     connection: keep-alive 
    content-length: 0
    date: Sat,27 Aug 2022 09:51:47 GMT
    keep-alive: timeout=60
    location: dbd55933-58f6-4220-a2e4-41f8aba89813

###Update recipe

`PUT /api/v1/recipes`


#### Request

    curl -X 'PUT' \
    'http://localhost:8080/api/v1/recipes/dbd55933-58f6-4220-a2e4-41f8aba89813' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{
    "name": "string",
    "instructions": "string",
    "type": "VEGETARIAN",
    "numberOfServing": 1,
    "ingredients": [
    "string"
    ]
    }'


#### Response [200]

    **RESPONSE HEADER**
    connection: keep-alive
    content-type: application/json
    date: Sat,27 Aug 2022 09:30:20 GMT
    keep-alive: timeout=60
    transfer-encoding: chunked 


###Delete recipe

`DELETE /api/v1/recipes`


#### Request

    curl -X 'DELETE' \
    'http://localhost:8080/api/v1/recipes/2b8b087d-1c07-4861-b4cf-22a2427ec8c4' \
    -H 'accept: application/json'


#### Response [204]

    **RESPONSE HEADER**
     connection: keep-alive 
    date: Sat,27 Aug 2022 09:59:17 GMT
    keep-alive: timeout=60  


