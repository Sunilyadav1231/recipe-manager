FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package
#ENV PORT 8080
#EXPOSE $PORT
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]

COPY ./target/recipe-manager-0.0.1-SNAPSHOT.jar recipe-manager-0.0.1-SNAPSHOT.jar
COPY ./src/main/resources/Schema.sql Schema.sql
COPY ./src/main/resources/application.yaml application.yml
CMD ["java","-jar","recipe-manager-0.0.1-SNAPSHOT.jar","-Dloader.path","/usr/src/app"]
#CMD ["java","-cp","recipe-manager-0.0.1-SNAPSHOT.jar","-Dloader.path","/usr/src/app/src/main/resources","org.springframework.boot.loader.PropertiesLauncher"]
